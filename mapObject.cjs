const testObject = 
{ name: 'Bruce Wayne', 
  age: 36,
  location: 'Gotham' 
};

let isObject=inp =>
  {
    if(inp==null)return false;
    else if(typeof inp == 'object')
    {
      return true;
    }
    return false;
  }

function cb(a)
{
return a+" HERO";
}


function mapObject(obj, cb)
{
   
        for( let i in obj)
        {
            let inp=obj[i];
    
            let answer=cb(inp);
            obj[i]=answer;
        }
        return obj;
    
}
module.exports=mapObject;