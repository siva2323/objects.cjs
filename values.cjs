const testObject = 
{ name: 'Bruce Wayne', 
  age: 36,
  location: 'Gotham' 
};


let isObject=inp =>
{
  if(inp==null)return false;
  else if(typeof inp == 'object')
  {
    return true;
  }
  return false;
}


let values=obj=>
  {
    if(isObject(obj))
    {
      return Object.values(obj);
    }
    else return [];
  }


  module.exports=values;