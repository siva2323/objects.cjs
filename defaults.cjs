const testObject = 
{ name: 'Bruce Wayne', 
  age: 36,
  location: 'Gotham' 
};
let isObject=inp =>
  {
    if(inp==null)return false;
    else if(typeof inp == 'object')
    {
      return true;
    }
    return false;
  }


const defaultProps={
    character: 'Batman'
};


let defaults=(obj,defaultProps) =>
{
  if(isObject(obj))
  {
    for(let a in defaultProps)
{
    obj[a]=defaultProps[a];


}
return obj;
  }
  else return [];


}
module.exports=defaults;