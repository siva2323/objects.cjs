const testObject = 
{ name: 'Bruce Wayne', 
  age: 36,
  location: 'Gotham' 
};

  let isObject=inp =>
  {
    if(inp==null)return false;
    else if(typeof inp == 'object')
    {
      return true;
    }
    return false;
  }

  let answer=[];
  let keys=obj=>
  {
    if(isObject(obj))
    {
      for(let prop in obj)
      {
        
        answer.push(prop);
      }
      return answer;
    }
    else return [];
  }


  module.exports=keys;
  